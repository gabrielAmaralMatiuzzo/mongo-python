from pymongo import MongoClient
from util import util
import pymongo.errors


def connect():
    try:
        conn = MongoClient("localhost", 27017)
        return conn
    except pymongo.errors as err:
        print(err)


def validateBanco(conn, banco):
    listBanco = conn.list_database_names()
    for item in listBanco:
        if item == banco:
            return True
    return False


def validateCollection(conn, banco, collection):
    listcollection = getBanco(conn, banco).list_collection_names()
    for item in listcollection:
        if item == collection:
            return True
    return False


def getBanco(conn, banco):
    if validateBanco(conn, banco):
        return conn[banco]
    else:
        return "banco not found"


def getCollection(conn, banco, collection):
    if validateCollection(conn, banco, collection):
        banco = conn[banco]
        return banco[collection]
    else:
        return "collection not found"


def insertDataCSV(conn, banco, collection, file):
    getCollection(conn, banco, collection).insert_many(util.csv_to_json(file))
