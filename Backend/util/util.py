import pandas as pd

def csv_to_json(filename):
    data = pd.read_csv(filename)
    return data.to_dict('records')
