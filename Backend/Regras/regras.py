import matplotlib.pyplot as plt


def qtdVendasPorGenero(collection, anoInicio, anoFim):
    # montando regras para aggregate
    agr = [{'$match': {'$and': [{'Year': {'$gte': anoInicio, '$lte': anoFim}}]}},
           {'$group': {'_id': '$Genre', 'sum': {'$sum': "$Global_Sales"}}},
           {'$sort': {'sum': -1}}]

    result = collection.aggregate(agr)
    # pegando os "_id" ou nomes do Genero
    nome = [result.next()['_id'], result.next()['_id'], result.next()['_id'], result.next()['_id'],
            result.next()['_id']]
    # resetando valores e pegando valores de quantidade de vendas
    result = collection.aggregate(agr)
    qtd = [result.next()['sum'], result.next()['sum'], result.next()['sum'], result.next()['sum'], result.next()['sum']]

    # preparando para exibir grafico
    plt.bar(nome, qtd)
    plt.ylabel('quantidade de vendas(em milhões)')
    plt.xlabel('plataforma')
    plt.title('Quantidade de venda por Genero de ' + str(anoInicio) + ' a ' + str(anoFim))
    plt.show()


def qtdVendasPorPlataforma(collection, anoInicio, anoFim):
    # montando regras para aggregate
    agr = [{'$match': {'$and': [{'Year': {'$gte': anoInicio, '$lte': anoFim}}]}},
           {'$group': {'_id': '$Platform', 'sum': {'$sum': "$Global_Sales"}}},
           {'$sort': {'sum': -1}}]

    result = collection.aggregate(agr)
    # pegando os "_id" ou nomes da plataforma
    nome = [result.next()['_id'], result.next()['_id'], result.next()['_id'], result.next()['_id'],
            result.next()['_id']]
    # resetando valores e pegando valores de quantidade de vendas
    result = collection.aggregate(agr)
    qtd = [result.next()['sum'], result.next()['sum'], result.next()['sum'], result.next()['sum'], result.next()['sum']]

    # preparando para exibir grafico
    plt.bar(nome, qtd)
    plt.ylabel('quantidade de vendas(em milhões)')
    plt.xlabel('plataforma')
    plt.title('Quantidade de venda por plataforma de ' + str(anoInicio) + ' a ' + str(anoFim))
    plt.show()


def vendasPlataformasPorRegiao(collection, regiao, qtd):
    agr = [{'$match': {'Year': {'$gte': 2015}}},
           {'$group': {'_id': '$Genre', 'sum': {'$sum': "$" + regiao + "_Sales"}}},
           {'$sort': {'sum': -1}},
           {'$limit': qtd}]

    return collection.aggregate(agr)


def vendasPlataformasPorRegiaoGrafic(collectionSale):
    NA = vendasPlataformasPorRegiao(collectionSale, "NA", 5)
    nome = [NA.next()['_id'], NA.next()['_id'], NA.next()['_id'], NA.next()['_id'], NA.next()['_id']]
    NA = vendasPlataformasPorRegiao(collectionSale, "NA", 5)
    qtd = [NA.next()['sum'], NA.next()['sum'], NA.next()['sum'], NA.next()['sum'], NA.next()['sum']]
    plt.bar(nome, qtd)
    plt.ylabel('quantidade de vendas(em milhões)')
    plt.xlabel('plataforma')
    plt.title('Quantidade de venda por genero na região NA')
    plt.show()

    EU = vendasPlataformasPorRegiao(collectionSale, "EU", 5)
    nome = [EU.next()['_id'], EU.next()['_id'], EU.next()['_id'], EU.next()['_id'], EU.next()['_id']]
    EU = vendasPlataformasPorRegiao(collectionSale, "EU", 5)
    qtd = [EU.next()['sum'], EU.next()['sum'], EU.next()['sum'], EU.next()['sum'], EU.next()['sum']]
    plt.bar(nome, qtd)
    plt.ylabel('quantidade de vendas(em milhões)')
    plt.xlabel('plataforma')
    plt.title('Quantidade de venda por genero na região EU')
    plt.show()

    JP = vendasPlataformasPorRegiao(collectionSale, "JP", 5)
    nome = [JP.next()['_id'], JP.next()['_id'], JP.next()['_id'], JP.next()['_id'], JP.next()['_id']]
    JP = vendasPlataformasPorRegiao(collectionSale, "JP", 5)
    qtd = [JP.next()['sum'], JP.next()['sum'], JP.next()['sum'], JP.next()['sum'], JP.next()['sum']]
    plt.bar(nome, qtd)
    plt.ylabel('quantidade de vendas(em milhões)')
    plt.xlabel('plataforma')
    plt.title('Quantidade de venda por genero na região JP')
    plt.show()

    Other = vendasPlataformasPorRegiao(collectionSale, "Other", 5)
    nome = [Other.next()['_id'], Other.next()['_id'], Other.next()['_id'], Other.next()['_id'], Other.next()['_id']]
    Other = vendasPlataformasPorRegiao(collectionSale, "Other", 5)
    qtd = [Other.next()['sum'], Other.next()['sum'], Other.next()['sum'], Other.next()['sum'], Other.next()['sum']]
    plt.bar(nome, qtd)
    plt.ylabel('quantidade de vendas(em milhões)')
    plt.xlabel('plataforma')
    plt.title('Quantidade de venda por genero na região Other')
    plt.show()


def vendasPlataformasGlobalGrafic(collectionSale):
    Global = vendasPlataformasPorRegiao(collectionSale, "Global", 5)
    nome = [Global.next()['_id'], Global.next()['_id'], Global.next()['_id'], Global.next()['_id'],
            Global.next()['_id']]
    Global = vendasPlataformasPorRegiao(collectionSale, "Global", 5)
    qtd = [Global.next()['sum'], Global.next()['sum'], Global.next()['sum'], Global.next()['sum'], Global.next()['sum']]
    plt.bar(nome, qtd)
    plt.ylabel('quantidade de vendas(em milhões)')
    plt.xlabel('plataforma')
    plt.title('Quantidade de venda por genero Global')
    plt.show()


def updatePlatformType(collectionSale, collectionPlatform):
    print("inserindo dados sobre as plataformas")
    for item in collectionPlatform.find({}, {'_id': 0, 'Platform': 1, 'Platform_Type': 1}):
        query = {'Platform': item['Platform']}
        value = {"$set": {"Platform_Type": item['Platform_Type']}}
        collectionSale.update_many(query, value)


def salePlatformType(collectionSale):
    agr = [{'$group': {'_id': '$Platform_Type', 'sum': {'$sum': "$Global_Sales"}}},
           {'$sort': {'sum': -1}}]

    result = collectionSale.aggregate(agr)
    labels = [result.next()['_id'], result.next()['_id'], result.next()['_id']]
    result = collectionSale.aggregate(agr)
    value = [result.next()['sum'], result.next()['sum'], result.next()['sum']]

    tot = 0.0
    for item in value:
        tot += item

    plt.pie(value, explode=None, labels=labels, colors=None,
            autopct=lambda p: '{:.0f}'.format(p * tot / 100), shadow=False, startangle=90)
    plt.axis('equal')
    plt.show()


def salePlatformTypePorRegiao(collection, regiao, qtd):
    agr = [{'$group': {'_id': '$Platform_Type', 'sum': {'$sum': "$" + regiao + "_Sales"}}},
           {'$sort': {'sum': -1}},
           {'$limit': qtd}]

    return collection.aggregate(agr)


def salePlatformTypeRegionGrafic(collectionSale):
    NA = salePlatformTypePorRegiao(collectionSale, "NA", 3)
    nome = [NA.next()['_id'], NA.next()['_id'], NA.next()['_id']]
    NA = salePlatformTypePorRegiao(collectionSale, "NA", 3)
    qtd = [NA.next()['sum'], NA.next()['sum'], NA.next()['sum']]
    plt.bar(nome, qtd)
    plt.ylabel('quantidade de vendas(em milhões)')
    plt.xlabel('plataforma')
    plt.title('Quantidade de venda por plataforma na região NA')
    plt.show()

    EU = salePlatformTypePorRegiao(collectionSale, "EU", 3)
    nome = [EU.next()['_id'], EU.next()['_id'], EU.next()['_id']]
    EU = salePlatformTypePorRegiao(collectionSale, "EU", 3)
    qtd = [EU.next()['sum'], EU.next()['sum'], EU.next()['sum']]
    plt.bar(nome, qtd)
    plt.ylabel('quantidade de vendas(em milhões)')
    plt.xlabel('plataforma')
    plt.title('Quantidade de venda por plataforma na região EU')
    plt.show()

    JP = salePlatformTypePorRegiao(collectionSale, "JP", 3)
    nome = [JP.next()['_id'], JP.next()['_id'], JP.next()['_id']]
    JP = salePlatformTypePorRegiao(collectionSale, "JP", 3)
    qtd = [JP.next()['sum'], JP.next()['sum'], JP.next()['sum']]
    plt.bar(nome, qtd)
    plt.ylabel('quantidade de vendas(em milhões)')
    plt.xlabel('plataforma')
    plt.title('Quantidade de venda por plataforma na região JP')
    plt.show()

    Other = salePlatformTypePorRegiao(collectionSale, "Other", 3)
    nome = [Other.next()['_id'], Other.next()['_id'], Other.next()['_id']]
    Other = salePlatformTypePorRegiao(collectionSale, "Other", 3)
    qtd = [Other.next()['sum'], Other.next()['sum'], Other.next()['sum']]
    plt.bar(nome, qtd)
    plt.ylabel('quantidade de vendas(em milhões)')
    plt.xlabel('plataforma')
    plt.title('Quantidade de venda por plataforma na região Other')
    plt.show()


def removeYearEmpty(collectionSale):
    collectionSale.remove({"Year": "N/A"})


def qtdVendaPorJogo10(collectionSale):
    Sales_Game_Global = collectionSale.aggregate(
        [{"$group": {"_id": "$Name", "per_Game_Global": {"$sum": "$Global_Sales"}}}, {"$sort": {"per_Game_Global": -1}},
         {"$limit": 10}])
    nome = [Sales_Game_Global.next()['_id'], Sales_Game_Global.next()['_id'], Sales_Game_Global.next()['_id'],
            Sales_Game_Global.next()['_id'], Sales_Game_Global.next()['_id']]
    Sales_Game_Global = collectionSale.aggregate(
        [{"$group": {"_id": "$Name", "per_Game_Global": {"$sum": "$Global_Sales"}}}, {"$sort": {"per_Game_Global": -1}},
         {"$limit": 10}])
    qtd = [Sales_Game_Global.next()['per_Game_Global'], Sales_Game_Global.next()['per_Game_Global'],
           Sales_Game_Global.next()['per_Game_Global'], Sales_Game_Global.next()['per_Game_Global'],
           Sales_Game_Global.next()['per_Game_Global']]
    plt.bar(nome, qtd)
    plt.ylabel('quantidade de vendas(em milhões)')
    plt.xlabel('jogos')
    plt.title('Quantidade de vendas por jogo - Global (10 primeiros)')
    plt.show()


def qtdVendaPorRegiao(collectionSale):
    NA = collectionSale.aggregate([{"$group": {"_id": "NA", "qtd": {"$sum": "$NA_Sales"}}}]).next()
    EU = collectionSale.aggregate([{"$group": {"_id": "EU", "qtd": {"$sum": "$EU_Sales"}}}]).next()
    JP = collectionSale.aggregate([{"$group": {"_id": "JP", "qtd": {"$sum": "$JP_Sales"}}}]).next()
    Other = collectionSale.aggregate([{"$group": {"_id": "Other", "qtd": {"$sum": "$Other_Sales"}}}]).next()
    nome = [NA['_id'], EU['_id'],
            JP['_id'], Other['_id']]
    qtd = [NA['qtd'], EU['qtd'],
           JP['qtd'], Other['qtd']]
    plt.bar(nome, qtd)
    plt.ylabel('quantidade de vendas(em milhões)')
    plt.xlabel('jogos')
    plt.title('Quantidade de vendas por região')
    plt.show()


def qtdVendaPorEmpresa(collectionSale):
    Sales_Publisher = collectionSale.aggregate(
        [{"$group": {"_id": "$Publisher", "per_Game_Publisher": {"$sum": "$Global_Sales"}}},
         {"$sort": {"per_Game_Publisher": -1}},
         {"$limit": 10}])
    nome = [Sales_Publisher.next()['_id'], Sales_Publisher.next()['_id'], Sales_Publisher.next()['_id'],
            Sales_Publisher.next()['_id'], Sales_Publisher.next()['_id']]
    Sales_Publisher = collectionSale.aggregate(
        [{"$group": {"_id": "$Publisher", "per_Game_Publisher": {"$sum": "$Global_Sales"}}},
         {"$sort": {"per_Game_Publisher": -1}},
         {"$limit": 10}])
    qtd = [Sales_Publisher.next()['per_Game_Publisher'], Sales_Publisher.next()['per_Game_Publisher'],
           Sales_Publisher.next()['per_Game_Publisher'], Sales_Publisher.next()['per_Game_Publisher'],
           Sales_Publisher.next()['per_Game_Publisher']]
    plt.bar(nome, qtd)
    plt.ylabel('quantidade de vendas(em milhões)')
    plt.xlabel('jogos')
    plt.title('Quantidade de vendas por empresa')
    plt.show()


def qtdVendaPorGenero(collectionSale):
    Sales_Genre = collectionSale.aggregate(
        [{"$group": {"_id": "$Genre", "per_Game_Genre": {"$sum": "$Global_Sales"}}}, {"$sort": {"per_Game_Genre": -1}},
         {"$limit": 10}])
    nome = [Sales_Genre.next()['_id'], Sales_Genre.next()['_id'], Sales_Genre.next()['_id'],
            Sales_Genre.next()['_id'], Sales_Genre.next()['_id']]
    Sales_Genre = collectionSale.aggregate(
        [{"$group": {"_id": "$Genre", "per_Game_Genre": {"$sum": "$Global_Sales"}}}, {"$sort": {"per_Game_Genre": -1}},
         {"$limit": 10}])
    qtd = [Sales_Genre.next()['per_Game_Genre'], Sales_Genre.next()['per_Game_Genre'],
           Sales_Genre.next()['per_Game_Genre'], Sales_Genre.next()['per_Game_Genre'],
           Sales_Genre.next()['per_Game_Genre']]
    plt.bar(nome, qtd)
    plt.ylabel('quantidade de vendas(em milhões)')
    plt.xlabel('jogos')
    plt.title('Quantidade de vendas por Genero')
    plt.show()


def qtdVendaPorJogo(collectionSale):
    Sales_Name = collectionSale.aggregate(
        [{"$group": {"_id": "$Name", "per_Game_Name": {"$sum": "$Global_Sales"}}}, {"$sort": {"per_Game_Name": -1}},
         {"$limit": 10}])
    nome = [Sales_Name.next()['_id'], Sales_Name.next()['_id'], Sales_Name.next()['_id'],
            Sales_Name.next()['_id'], Sales_Name.next()['_id']]
    Sales_Name = collectionSale.aggregate(
        [{"$group": {"_id": "$Name", "per_Game_Name": {"$sum": "$Global_Sales"}}}, {"$sort": {"per_Game_Name": -1}},
         {"$limit": 10}])
    qtd = [Sales_Name.next()['per_Game_Name'], Sales_Name.next()['per_Game_Name'],
           Sales_Name.next()['per_Game_Name'], Sales_Name.next()['per_Game_Name'],
           Sales_Name.next()['per_Game_Name']]
    plt.bar(nome, qtd)
    plt.ylabel('quantidade de vendas(em milhões)')
    plt.xlabel('jogos')
    plt.title('Quantidade de vendas por Jogo')
    plt.show()
