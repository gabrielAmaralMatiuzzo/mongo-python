from mongo import Connector
from Regras import regras

conn = Connector.connect()

collectionSale = Connector.getCollection(conn, "Games", "Sale")
collectionPlataforma = Connector.getCollection(conn, "Games", "Plataforma")

if collectionSale.count_documents({}) == 0:
    Connector.insertDataCSV(conn, "Games", "Sale", "./vgsales.csv")
    regras.removeYearEmpty(collectionSale)
    print("preenchendo dados de Sale")
    Connector.insertDataCSV(conn, "Games", "Plataforma", "./Plataforma.csv")
    print("preenchendo dados de Plataforma")

# A.1
# regras.qtdVendasPorGenero(collectionSale, 1980, 1989)
# regras.qtdVendasPorGenero(collectionSale, 1990, 1999)
# regras.qtdVendasPorGenero(collectionSale, 2000, 2009)
# regras.qtdVendasPorGenero(collectionSale, 2010, 2020)

# A.2
# regras.qtdVendasPorPlataforma(collectionSale, 1980, 1989)
# regras.qtdVendasPorPlataforma(collectionSale, 1990, 1999)
# regras.qtdVendasPorPlataforma(collectionSale, 2000, 2009)
# regras.qtdVendasPorPlataforma(collectionSale, 2010, 2020)

# B.1
# regras.qtdVendaPorRegiao(collectionSale)

# B.2
# regras.qtdVendaPorEmpresa(collectionSale)

# B.3
# regras.qtdVendaPorGenero(collectionSale)

# B.4
# regras.qtdVendaPorJogo(collectionSale)

# C.1
# regras.qtdVendasPorGenero(collectionSale, 1980, 2000)
# regras.qtdVendasPorGenero(collectionSale, 2000, 2020)

# C.2
# regras.vendasPlataformasPorRegiaoGrafic(collectionSale)

# C.3
# regras.vendasPlataformasGlobalGrafic(collectionSale)

# d.1
# regras.updatePlatformType(collectionSale, collectionPlataforma)

# d.2
# regras.salePlatformType(collectionSale)

# d.3
regras.salePlatformTypeRegionGrafic(collectionSale)
